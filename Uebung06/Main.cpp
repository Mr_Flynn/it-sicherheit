#include <random>
#include <set>
#include <cmath>
#include <iostream>

std::random_device rnd;

typedef long long int64;
typedef long double ldouble;

ldouble kollisionProbability(int64 m, int64 n)
{
	ldouble probability = 1;

	ldouble resultAmount = powl(2, n);

	if (m <= resultAmount)
	{
		ldouble invertedProbability = 1;

		for (int64 a = 0; a < m; a++)
			invertedProbability *= (resultAmount - a) / resultAmount;

		probability -= invertedProbability;
	}

	return probability;
}

int64 generate(int64 n)
{
	int64 retval = 0;
	int64 entropy = rnd.entropy();
	int64 mask = 0xffffffffffffffff >> (64 - n);
	for (int64 a = 64; a > 0; a -= entropy)
		retval = (retval << entropy) + rnd();
	retval &= mask;

	//for (int64 a = 0; a < n; a++)
	//	retval = (retval << 1) + rnd() % 2;

	return retval;
}

int64 analyze(int64 m, int64 n)
{
	bool result = false;

	std::set<int64> generated;

	for (int64 a = 1; a <= m; a++)
	{
		generated.insert(generate(n));
		if (result = generated.size() != a)
			break;
	}

	return result;
}

ldouble stochasticProbability(int64 m, int64 n)
{
	ldouble retval = 0;
	int64 testAmount = 100000;

	for (int64 a = 0; a < testAmount; a++)
		retval += analyze(m, n);

	return retval / (ldouble)testAmount;
}

void testScenario(int64 m, int64 n)
{
	std::cout << "m:            " << m << std::endl;
	std::cout << "n:            " << n << std::endl;
	std::cout << "zahlenraum:   " << powl(2, n) << std::endl;
	std::cout << "rechnerisch:  " << kollisionProbability(m, n) * 100 << "%" << std::endl;
	std::cout << "stochastisch: " << stochasticProbability(m, n) * 100 << "%" << std::endl;
	std::cout << std::endl;
}

ldouble test(ldouble n, ldouble last, ldouble next, int64 precision)
{
	ldouble distance = next - last;
	ldouble retval = last;
	if (precision > 0 && roundl(last) != roundl(next))
		for (ldouble loop = last, increment = 1, prev = 0; loop < next; prev = loop, loop += increment, increment *= 2)
			if (kollisionProbability(loop, n) >= 0.5)
			{
				retval = test(n, prev, loop, --precision);
				break;
			}
	return retval;
}

ldouble percent(int64 n)
{
	return test(n, 0, powl(2, n), 10000) + 1;
}

int main()
{
	std::cout << percent(4) << std::endl;
	std::cout << percent(10) << std::endl;
	std::cout << percent(16) << std::endl;
	std::cout << percent(128) << std::endl;


	testScenario(2, 2);
	testScenario(3, 2);
	testScenario(4, 2);
	testScenario(5, 10);
	testScenario(50, 10);
	testScenario(100, 10);
	testScenario(50, 16);
	testScenario(100, 16);
	testScenario(5, 32);

	int wait;
	std::cin >> wait;
	return 0;
}