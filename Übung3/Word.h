#pragma once
#include <string>
#include "Alphabet.h"

using namespace std;

class Word
{
	string word;
	Alphabet* alphabet;
	char* convertWord(string* word);
public:
	static char* getNextWord(char* currentWord, char* alphabet);
	static char* getPreviousWord(char* currentWord, char* alphabet);
	Word(string word, Alphabet* alphabet);
	char* getNextWord();
	char* getWord();
	char* getPreviousWord();
	int length();

	friend bool operator< (Word w1, Word w2)
	{
		bool retval = w1.length() <= w2.length() && *w1.alphabet == *w2.alphabet;

		if (retval && w1.length() == w2.length())
		{
			retval = false;
			string s1 = w1.getWord(), s2 = w2.getWord();

			for (int a = 0; a < s1.length(); a++)
			{
				if (w1.word[a] != w2.word[a])
				{
					retval = w1.alphabet->charIndex(w1.word[a]) < w2.alphabet->charIndex(w2.word[a]);
					break;
				}
			}
		}

		return retval;
	}

	friend bool operator== (Word w1, Word w2)
	{
		return w1.word == w2.word && *w1.alphabet == *w2.alphabet;
	}

	friend bool operator!= (Word w1, Word w2)
	{
		return !(w1 == w2);
	}

	friend bool operator> (Word w1, Word w2)
	{
		return !(w1 < w2 || w1 == w2);
	}

	friend bool operator<= (Word w1, Word w2)
	{
		return !(w1 > w2);
	}

	friend bool operator>= (Word w1, Word w2)
	{
		return !(w1 < w2);
	}
};

