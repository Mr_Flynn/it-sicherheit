#include "Word.h"

char* Word::getNextWord(char* currentWord, char* alphabet)
{
	Alphabet alph(alphabet);
	Word word(currentWord, &alph);
	return word.getNextWord();
}

char* Word::getPreviousWord(char* currentWord, char* alphabet)
{
	Alphabet alph(alphabet);
	Word word(currentWord, &alph);
	return word.getPreviousWord();
}

Word::Word(string word, Alphabet* alphabet)
{
	this->alphabet = alphabet;
	this->word = word;
}

char* Word::getNextWord()
{
	string next(word);

	bool overflow;
	int index = next.length() - 1;

	do
	{
		next[index] = alphabet->nextChar(next[index]);
		overflow = alphabet->hasOverflow();
		index--;
	} while (overflow && index > -1);

	if (overflow)
		next = alphabet->first() + next;

	return convertWord(&next);
}

char* Word::getPreviousWord()
{
	string previous(word);

	bool underflow;
	int index = previous.length() - 1;

	do
	{
		previous[index] = alphabet->previousChar(previous[index]);
		underflow = alphabet->hasUnderflow();
		index--;
	} while (underflow && index > -1);

	if (underflow)
	{
		if (previous.length() > 1)
			previous = previous.substr(1);
		else
			previous = alphabet->first();
	}

	return convertWord(&previous);
}

char* Word::getWord()
{
	return convertWord(&word);
}

char* Word::convertWord(string* word)
{
	char* retval = new char[word->length() + 1];
	memcpy(retval, word->c_str(), (word->length() + 1) * sizeof(char));
	return retval;
}

int Word::length()
{
	return word.length();
}