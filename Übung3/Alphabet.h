#pragma once
#include <string>
#include <map>

using namespace std;

class Alphabet
{
	class Char
	{
	public:
		char next;
		char previous;
		int index;
		Char() : Char(-1, 0, 0) { }
		Char(int index, char previous, char next)
		{
			this->index = index;
			this->previous = previous;
			this->next = next;
		}
	};

	string alphabet;
	map<char, Char> charMapping;
	bool overflow = false;
	bool underflow = false;
	void removeDuplicates();
	void mapAlphabet();
public:
	Alphabet(string alphabet);
	char first();
	char last();
	char nextChar(char current);
	char previousChar(char current);
	int charIndex(char current);
	bool hasOverflow();
	bool hasUnderflow();

	friend bool operator== (Alphabet a1, Alphabet a2)
	{
		bool retval = a1.alphabet.length() == a2.alphabet.length();

		if (retval)
		{
			for (int a = 0; a < a1.alphabet.length(); a++)
				if (a1.alphabet[a] != a2.alphabet[a])
				{
					retval = false;
					break;
				}
		}

		return retval;
	}
};