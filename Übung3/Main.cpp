#include "Word.h"
#include "time.h"
#include <iostream>
#include <stdio.h>

int main()
{
	const char* alphabet = "ab2\0";
	const char* word = "a\0";
	char* temp;

	Alphabet a(alphabet);
	Word w(word, &a);

	for (int i = 0; i < 125; i++)
	{
		cout << (temp = w.getWord());
		w = Word(w.getNextWord(), &a);
		cout << " is lower than " << w.getWord() << ": " << (Word(temp, &a) < w) << endl;
		delete temp;
	}

	cout << endl;

	for (int i = 0; i < 128; i++)
	{
		temp = w.getWord();
		w = Word(w.getPreviousWord(), &a);
		cout << temp << " is greater than " << w.getWord() << ": " << (Word(temp, &a) > w) << endl;
		delete temp;
	}

	cout << endl;

	cout << "a is greater than bb: " << (Word("a", &a) > Word("bb", &a)) << endl;
	cout << "aa is lower than b: " << (Word("aa", &a) < Word("b", &a)) << endl;
	cout << "aa equals aa: " << (Word("aa", &a) == Word("aa", &a)) << endl;
	cout << "aa not equals ba: " << (Word("aa", &a) != Word("ba", &a)) << endl << endl;


	int anount = 10000000;
	clock_t start = clock();

	w = Word(word, &a);
	for (int i = 0; i < anount; i++)
	{
		w = Word(temp = w.getNextWord(), &a);
		delete temp;
	}

	cout << ((double)(clock() - start) / (double)CLOCKS_PER_SEC) << " s for " << anount << " runs" << endl;
	cout << "start wprd: " << word << endl << "last word: " << w.getWord() << endl;


	getchar();
}