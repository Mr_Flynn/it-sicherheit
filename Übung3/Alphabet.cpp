#include "Alphabet.h"

Alphabet::Alphabet(string alphabet)
{
	this->alphabet = alphabet;

	removeDuplicates();

	mapAlphabet();
}

char Alphabet::first()
{
	return alphabet[0];
}

char Alphabet::last()
{
	return alphabet[alphabet.length() - 1];
}

char Alphabet::nextChar(char current)
{
	char retval = charMapping[current].next;
	underflow = false;
	overflow = retval == first();
	return retval;
}

char Alphabet::previousChar(char current)
{
	char retval = charMapping[current].previous;
	overflow = false;
	underflow = retval == last();
	return retval;
}

int Alphabet::charIndex(char current)
{
	return charMapping[current].index;
}

bool Alphabet::hasOverflow()
{
	return overflow;
}

bool Alphabet::hasUnderflow()
{
	return underflow;
}

void Alphabet::removeDuplicates()
{
	map<char, char> existing;

	string cutAlphabet;

	int length = 0;
	for (int a = 0; a < alphabet.length(); a++)
	{
		existing[alphabet[a]] = 0;
		if (existing.size() != length)
		{
			length++;
			cutAlphabet += alphabet[a];
		}
	}

	alphabet = cutAlphabet;
}

void Alphabet::mapAlphabet()
{
	switch (alphabet.length())
	{
	case 0:
		throw new exception("alphabet empty");
		break;
	case 1:
		charMapping[alphabet[0]] = Char(0, alphabet[0], alphabet[0]);
	default:
		int length = alphabet.length() - 1;
		charMapping[alphabet[length]] = Char(length, alphabet[length - 1], alphabet[0]);
		for (int a = length - 1; a > 0; a--)
			charMapping[alphabet[a]] = Char(a, alphabet[a - 1], alphabet[a + 1]);
		charMapping[alphabet[0]] = Char(0, alphabet[length], alphabet[1]);
		break;
	}
}