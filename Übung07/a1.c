#include <stdio.h>


unsigned int* stackValPtr(int val)
{
    //get stack base pointer of current frame
    register unsigned int * bp asm ("bp");

    //get stack base pointer of caller frame
    unsigned int* stack_ptr = *bp;
    //get stack base pointer of variable owner frame
    stack_ptr = *stack_ptr;

    do
    {
        stack_ptr--;
    } while (*stack_ptr != val);

    return stack_ptr;
}

int g(int x) 
{
    //skip undesired values
    //0: exists too often on stack
    // < -1: unnecessary to check for these values
    // > 1:  unnecessary to check for these values
    if (x == 0 || x > 1 || x < -1)
        return 0;

    unsigned int* x_ptr = stackValPtr(x);

    //makes f() loop > 2000000000 times
    if (x == 1)
        *x_ptr = -2000000000;
    //prevents endless loop
    else if (x == -1)
    {
        *x_ptr = 2;
        *stackValPtr(7) = 42;
    }

    return x_ptr;
}


void f() {
    int y = 7;
    int i = 0;
    for (i = 0; i < 10; i++) {
        if (i < -1) i = -1;
        printf("i: %d, g(i): %d\n", i, g(i));
    }
    printf("value of y: %d\n", y);
}



int main()
{
    f();
    return 0;
}