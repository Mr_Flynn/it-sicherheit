#include <stdio.h>
#include <math.h>

void evil() {
    printf("i am evil\n");
}

void q() {
    // bitte modifizieren Sie nur im Bereich unterhalb dieser Zeile
    
    //tested with gcc

    //get base pointer
    register unsigned int* bp asm ("bp");
    //get return address
    unsigned int* stack_ptr = bp + 2;
    //get pointer to evil
    void (*evil_ptr)() = evil;
    //write pointer to evil into return address
    *stack_ptr = evil_ptr;
    
    // bitte modifizieren Sie nur im Bereich oberhalb dieser Zeile
    printf("q is done \n");
}

void harmless() {
    q();
}

int main() {
    harmless();
    return 0;
}