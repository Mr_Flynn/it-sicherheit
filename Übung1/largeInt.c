#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "largeInt.h"





/**
 ** Returns the number of leading zeroes of the given argument.
 **
 ** A leading zero is a binary digit of the given arg that is set
 ** to 0 and to the left of which only appear 0's.
 **/
uint8 GetNumberOfLeadingZeroes(uint32 arg) {
    uint32 mask = 0x80000000U;
    uint8 count = 0;

    while (((arg & mask) == 0) && (count < 32)) {
        mask = mask >> 1;
        count++;
    }
    return count;
}


/**
 ** \brief Checks for leading zeroes and, based on the number of these,
 **        computes b->usedWords and b->bitSize.
 **
 ** \param[in] b The BigInt, for which to compute the usage values.
 **/
void RecomputeUsageVariables(LargeInt* b) {
    uint32 i;
    uint32* dataPointer;

    i = b->wordSize - 1;
    dataPointer = &(b->data[i]);

    while (((*dataPointer) == 0) && (i>0)) {
        i--;
        dataPointer--;
    }
    
    b->bitSize = 32 - GetNumberOfLeadingZeroes((*dataPointer)) + (i * BITSPERWORD);
    if (b->bitSize == 0) {
        b->usedWords = 0;
    } else {
        b->usedWords = i + 1;
    }
}


/**
 ** Initializes a new LargeInt with the given integer value.
 **
 ** \param[in] value The value that is to be passed to the new LargeInt.
 ** \param[wordSize] The number of 32-Bit-words that shall used in the new LargeInt.
 ** \return A LargeInt that has been initialized with the given value.
 **/
LargeInt* InitLargeIntWithUint32(uint32 value, uint8 wordSize) {
    LargeInt* x = (LargeInt*)calloc(1, sizeof(LargeInt));
    x->data = (uint32*)calloc(wordSize, sizeof(uint32));
    x->wordSize = wordSize;
    
    if (value == 0) {
        x->usedWords = 0;
        x->bitSize = 0;
    } else {
        x->usedWords = 0;
        x->bitSize = 32 - GetNumberOfLeadingZeroes(value);
        while (value > 0) {
            x->data[x->usedWords] = value & STANDARD_USEBIT_MASK;   
            x->usedWords++;
            value = value >> BITSPERWORD;
        } 
    }
    return x;
}


/**
 * Frees the memory of the given LargeInt.
 */
void freeLargeInt(LargeInt* x) {
    free(x->data);
    free(x);
}


void printLargeInt(LargeInt* x) {
    uint32 i = x->bitSize - 1;
    while (i < (uint32)-1) {
        uint32 wordIndex = i / BITSPERWORD;
        uint32 bitIndex = i % BITSPERWORD;
        uint32 testBit = 1 << bitIndex;
        if ((x->data[wordIndex] & testBit) != 0) {
            printf("1");
        }
        else {
            printf("0");
        }
        i--;
    }
    printf("\n");
}




/**
 ** Returns TRUE if and only if the given LargeInt is even.
 **/
boolean IsEven(const LargeInt* b) {
    return !(b->data[0] & 1U);
}

/**
 ** Returns TRUE if and only if the given LargeInt is odd.
 **/
boolean IsOdd(const LargeInt* b) {
    return b->data[0] & 1U;
}

/**
 ** Returns TRUE if words in use are equal
 ** Does NOT compare wordSize attribute of elements
 **/
boolean Equals(const LargeInt* e1, const LargeInt* e2)
{
    boolean retval;

    if (retval = (e1->usedWords == e2->usedWords) & TRUE)
    {
        for (uint32 a = 0; a < e2->usedWords; a++)
            if (e1->data[a] != e2->data[a])
            {
                retval = FALSE;
                break;
            }
    }

    return retval;
}

/**
 ** \brief Adds the two given summands and returns the result.
 **
 ** This algorithm treats the arguments as if they were both
 ** positive, i.e. the sign of the integers is ignored.
 **
 ** \param[in] s1 The first summand.
 ** \param[in] s2 The second summand.
 ** \result The sum of s1 and s2. The word size of the result
 **         is exactly as large as needed to hold the sum.
 **
 **/
LargeInt* Add(const LargeInt* s1, const LargeInt* s2) {
    //distinguish longer and shorter number
    uint32 *shorterData, *longerData;
    uint32 minLength, maxLength;

    if (s1->usedWords > s2->usedWords)
    {
        minLength = s2->usedWords;
        shorterData = s2->data;

        maxLength = s1->usedWords;
        longerData = s1->data;
    }
    else
    {
        minLength = s1->usedWords;
        shorterData = s1->data;

        maxLength = s2->usedWords;
        longerData = s2->data;
    }

    //init retval
    LargeInt* retval = InitLargeIntWithUint32(0U, maxLength + 1);

    //add shorter and longer number words into retval
    uint32 overflow = 0;
    uint32 index = 0;
    while (index < minLength)
    {
        //add summands
        retval->data[index] += shorterData[index] + longerData[index] + overflow;

        //calculate overflow
        overflow = (retval->data[index] & STANDARD_CALCBIT_MASK) >> BITSPERWORD;
        //reset overflow bits in current word
        retval->data[index] = retval->data[index++] & STANDARD_USEBIT_MASK;
    }
    
    //copy remaining words in longer number into retval
    while (index < maxLength)
    {
        retval->data[index] += longerData[index] + overflow;

        overflow = (retval->data[index] & STANDARD_CALCBIT_MASK) >> BITSPERWORD;
        retval->data[index] = retval->data[index++] & STANDARD_USEBIT_MASK;
    }

    //append last overflow
    retval->data[index] = overflow;

    //sets retval attributes
    RecomputeUsageVariables(retval);

    return retval;
}

LargeInt* Multiply(const LargeInt* m1, const LargeInt* m2) {
    LargeInt* retval = InitLargeIntWithUint32(0, m1->usedWords + m2->usedWords);

    uint32 overflow;
    uint32 wordIndex = 0;
    for (uint32 outer = 0; outer < m1->usedWords; outer++)
    {
        overflow = 0;

        for (uint32 inner = 0; inner < m2->usedWords; inner++)
        {
            wordIndex = outer + inner;
            retval->data[wordIndex] += m1->data[outer] * m2->data[inner] + overflow;

            overflow = (retval->data[wordIndex] & STANDARD_CALCBIT_MASK) >> BITSPERWORD;
            retval->data[wordIndex] = retval->data[wordIndex] & STANDARD_USEBIT_MASK;
        }

        retval->data[++wordIndex] = overflow;
    }

    RecomputeUsageVariables(retval);

    return retval;
}




void OddEvenTests()
{
    LargeInt* odd = InitLargeIntWithUint32(1U, 1);
    LargeInt* even = InitLargeIntWithUint32(2U, 1);
    printf("is 1 an odd number: %s\n", IsOdd(odd) ? "true" : "false");
    printf("is 2 an odd number: %s\n", IsOdd(even) ? "true" : "false");
    printf("is 1 an even number: %s\n", IsEven(odd) ? "true" : "false");
    printf("is 2 an even number: %s\n", IsEven(even) ? "true" : "false");

    freeLargeInt(odd);
    freeLargeInt(even);
}

void EqualsTest()
{
    LargeInt* copy, * temp;
    copy = temp = InitLargeIntWithUint32(1, 1);
    printf("1 equals 1: %s\n", Equals(temp, temp) ? "true" : "false");

    temp = InitLargeIntWithUint32(2, 1);
    printf("1 equals 2: %s\n", Equals(copy, temp) ? "true" : "false");

    freeLargeInt(copy);
    freeLargeInt(temp);
}

void AddTest()
{
    LargeInt* x = InitLargeIntWithUint32(70000, 5);
    LargeInt* y = InitLargeIntWithUint32(80000, 5);
    LargeInt* copy;
    LargeInt* z = Add(copy = x, y);
    printLargeInt(x);
    printf("+\n");
    printLargeInt(y);
    printf("equals\n");
    printLargeInt(x = InitLargeIntWithUint32(150000, 6));
    printf(" => %s\n", Equals(z, x) ? "true" : "false");

    freeLargeInt(x);
    freeLargeInt(y);
    freeLargeInt(z);
    freeLargeInt(copy);
}


void MultiplyTest()
{
    LargeInt* copy, * temp;
    copy = temp = InitLargeIntWithUint32(0xffffU, 1);
    temp = Multiply(temp, temp);
    freeLargeInt(copy);
    copy = temp;
    temp = InitLargeIntWithUint32(0xfffe0001U, 2);
    printf("0xffffU * 0xffffU equals 0xfffe0001U: %s\n", Equals(copy, temp) ? "true" : "false");

    freeLargeInt(copy);
    freeLargeInt(temp);
}

void BenchmarkTest(LargeInt* (* benchmark)())
{
    LargeInt* large;

    clock_t runTime, printTime;
    
    runTime = clock();
    large = benchmark();
    runTime = clock() - runTime;

    printTime = clock();
    printLargeInt(large);
    printTime = clock() - printTime;

    printf("\nnummer ist %d bit lang\n", large->bitSize);
    printf("berechnungsdauer: %f s\n", runTime / (float)CLOCKS_PER_SEC);
    printf("ausgabedauer: %f s\n", printTime / (float)CLOCKS_PER_SEC);
    printf("equals test (nummer == nummer): %s\n", Equals(large, large) ? "true" : "false");

    freeLargeInt(large);
}

LargeInt* AddBenchmark()
{
    LargeInt* large, * copy, * temp;
    large = InitLargeIntWithUint32(2U, 2);
    temp = InitLargeIntWithUint32(1U, 1);


    while (large->wordSize < 10000)
    {
        copy = large;
        large = Add(large, large);
        freeLargeInt(copy);

        copy = large;
        large = Add(large, temp);
        freeLargeInt(copy);

        copy = large;
        large = Add(large, large);
        freeLargeInt(copy);
    }

    freeLargeInt(temp);

    return large;
}

LargeInt* MultiplyBenchmark()
{
    LargeInt* large, * copy;
    large = InitLargeIntWithUint32(0xffffffffU, 2);

    /*
    * nummer ist 16777216 bit lang
    * berechnungsdauer: 1945.729004 s
    * ausgabedauer: 403.484009 s
    * equals test (nummer == nummer): true
    * while (large->wordSize < 1000000)
    */
    while (large->wordSize < 100000)
    {
        copy = large;
        large = Multiply(large, large);
        freeLargeInt(copy);
    }

    return large;
}



// Verstehen Sie die untige main-Funktion bitte als Anstoß zum 
// Testen Ihres Codes. Fügen Sie weitere, sinnvolle Tests hinzu!
int main() {
    OddEvenTests();
    printf("\n");

    EqualsTest();
    printf("\n");

    AddTest();
    printf("\n");

    MultiplyTest();
    printf("\n");

    BenchmarkTest(&AddBenchmark);
    printf("\n");

    BenchmarkTest(&MultiplyBenchmark);

    getchar();

    return 0;
}